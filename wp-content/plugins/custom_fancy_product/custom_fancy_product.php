<?php
/**
 * Plugin Name:       Custom Calculator
 * Plugin URI:        https://logicsbuffer.com/
 * Description:       Custom Calculator for ACE Banners Printing [custom-calculator]
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Mustafa jamal
 * Author URI:        https://logicsbuffer.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       custom-calculator
 * Domain Path:       /languages
 */


add_action( 'init', 'custom_fancy_product' );

function custom_fancy_product() {

	add_shortcode( 'custom-calculator', 'custom_fancy_product_form_single' );
	add_action( 'wp_enqueue_scripts', 'custom_fancy_product_script' );
	// Setup Ajax action hook
	add_action( 'wp_ajax_read_me_later', array( time(), 'read_me_later' ) );
	add_action( 'wp_ajax_nopriv_read_me_later', array( time(), 'read_me_later' ) );
}



 function custom_fancy_product_script() {
		        
	wp_enqueue_style( 'rt_custom_fancy_style', plugins_url().'/custom_fancy_product/css/custom_fancy.css');
}



//Test

//add_action( 'woocommerce_before_calculate_totals', 'add_custom_price' );

// add_action( 'woocommerce_before_calculate_totals', 'add_custom_price' );
// function add_custom_price( $cart_object ) {
// 	global $post;
// 	$product_id = $post->ID;    
//     $myPrice = get_post_meta('total_price_quote');

//     // Get the WC_Product Object instance
// 	$product = wc_get_product( $product_id );

// 	// Set the product active price (regular)
// 	$product->set_price( $myPrice );
// 	$product->set_regular_price( $myPrice ); // To be sure

// 	// Save product data (sync data and refresh caches)
// 	$product->save();

// }
//Test
// function woocommerce_custom_price_to_cart_item( $cart_object ) {  
//     if( !WC()->session->__isset( "reload_checkout" )) {
//         foreach ( $cart_object->cart_contents as $key => $value ) {
//             if( isset( $value["custom_price"] ) ) {
//                 //for woocommerce version lower than 3
//                 //$value['data']->price = $value["custom_price"];
//                 //for woocommerce version +3
//                 $value['data']->set_price($value["custom_price"]);
//             }
//         }  
//     }  
// }
// add_action( 'woocommerce_before_calculate_totals', 'woocommerce_custom_price_to_cart_item', 99 );

function read_me_later(){
			

			$total_price = $_REQUEST['total_price'];
			update_post_meta('total_price_quote', $total_price);
			
			// global $post;
			// global $woocommerce;

			// $product_id = $post->ID;
			// echo $product_id;
			// WC()->cart->add_to_cart($product_id);
// Simple, grouped and external products
// add_filter('woocommerce_product_get_price', array( $this, 'custom_price' ), 99, 2 );
// add_filter('woocommerce_product_get_regular_price', array( $this, 'custom_price' ), 99, 2 );

}


//Custom Price Functions

//Custom Price Functions



function custom_fancy_product_form_single() {
				

				wp_enqueue_script( 'rt_custom_fancy_script', plugins_url().'/custom_fancy_product/js/custom_fancy1.js',array(),time());	
				$plugins_url =  plugins_url( 'images/icons/', __FILE__ );
				global $post;
				global $woocommerce;
				$product_id = $post->ID;

				if(isset($_POST['submit_prod'])){
						
					//$total_qty = $_POST['rt_qty'];			 
					//$quantity = 10;			 
					//update_post_meta($new_post_id, 'width_finishing', $rt_finishing);
					$rt_total_price = $_POST['pricetotal_quote'];
					
					echo $rt_total_price;
					echo $product_id;
					
					//Set price
					global $post;
					$product_id = $post->ID;    
				    // $myPrice = get_post_meta('total_price_quote');

				    // Get the WC_Product Object instance
					// $product = wc_get_product( $product_id );

					// Set the product active price (regular)
					// $product->set_price( $myPrice );
					// $product->set_regular_price( $myPrice ); // To be sure

					// Save product data (sync data and refresh caches)
					//$product->save();
					//Set price end

					//die();
					// Cart item data to send & save in order
					//  $cart_item_data = array('custom_price' => $rt_total_price);   
					// // woocommerce function to add product into cart check its documentation also 
					// // what we need here is only $product_id & $cart_item_data other can be default.
					//  WC()->cart->add_to_cart( $product_id, $cart_item_data);
					// // Calculate totals
					//  WC()->cart->calculate_totals();
					// //  Save cart to session
					//  WC()->cart->set_session();
					// // Maybe set cart cookies
					//  WC()->cart->maybe_set_cart_cookies();

					//add_filter( 'woocommerce_add_cart_item' , 'set_woo_prices');
					//add_filter( 'woocommerce_get_cart_item_from_session',  'set_session_prices', 20 , 3 );
					

				
				}


				// function set_woo_prices( $woo_data ) {
				//   if ( ! isset( $_POST['pricetotal_quote'] ) || empty ( $_POST['pricetotal_quote'] ) ) { return $woo_data; }
				//   $woo_data['data']->set_price( $_POST['pricetotal_quote'] );
				//   $woo_data['my_price'] = $_POST['pricetotal_quote'];
				//   return $woo_data;
				// }

				// function  set_session_prices ( $woo_data , $values , $key ) {
				//     if ( ! isset( $woo_data['my_price'] ) || empty ( $woo_data['my_price'] ) ) { return $woo_data; }
				//     $woo_data['data']->set_price( $woo_data['my_price'] );
				//     return $woo_data;
				// }



				ob_start();
				//$page_title = get_the_title();
				//$terms = get_the_terms( get_the_ID(), 'product_cat' );
				//$product_type = $terms[0]->slug;
				?>
				<script type="text/javascript">
				    var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
				</script>				
<div class="custom_calculator single_product">
	<h3 class="calculator_title">Price Calculator</h3>
	<div class="orderform" id="form1">
	
		<form role="form" action="" method="post" id="add_cart" enctype="multipart/form-data" method="post">						
					    						
				<!-- <span id="price"/></span> -->
				<input type="hidden" id="eprice" />
				<input type="hidden" id="hprice" />						
				<input type="hidden" id="psqft" />						
				<input type="hidden" id="efinal" />
				<input type="hidden" id="qfinal" />
				<input type="hidden" id="sqft_total" />
				<input type="hidden" id="eyeletsT1" />
				<input type="hidden" id="hammingT1" />


			<?php  //$ace_sqfoot = get_post_meta(get_the_ID(), 'units', true); ?>
			
			<!-- Inches get value form fields -->
			<?php  $inchesquantity_1 = get_post_meta(get_the_ID(), 'inchesquantity_1', true); ?>
			<?php  $inchesprice_1 = get_post_meta(get_the_ID(), 'inchesprice_1', true); ?>

			<?php  $inchesquantity_2 = get_post_meta(get_the_ID(), 'inchesquantity_2', true); ?>
			<?php  $inchesprice_2 = get_post_meta(get_the_ID(), 'inchesprice_2', true); ?>

			<?php  $inchesquantity_3 = get_post_meta(get_the_ID(), 'inchesquantity_3', true); ?>
			<?php  $inchesprice_3 = get_post_meta(get_the_ID(), 'inchesprice_3', true); ?>

			<?php  $inchesquantity_4 = get_post_meta(get_the_ID(), 'inchesquantity_4', true); ?>
			<?php  $inchesprice_4 = get_post_meta(get_the_ID(), 'inchesprice_4', true); ?>

			<?php  $inchesquantity_5 = get_post_meta(get_the_ID(), 'inchesquantity_5', true); ?>
			<?php  $inchesprice_5 = get_post_meta(get_the_ID(), 'inchesprice_5', true); ?>
			
			<!-- Foot get value form fields -->
			<?php  $footquantity_1 = get_post_meta(get_the_ID(), 'footquantity_1', true); ?>
			<?php  $footprice_1 = get_post_meta(get_the_ID(), 'footprice_1', true); ?>

			<?php  $footquantity_2 = get_post_meta(get_the_ID(), 'footquantity_2', true); ?>
			<?php  $footprice_2 = get_post_meta(get_the_ID(), 'footprice_2', true); ?>

			<?php  $footquantity_3 = get_post_meta(get_the_ID(), 'footquantity_3', true); ?>
			<?php  $footprice_3 = get_post_meta(get_the_ID(), 'footprice_3', true); ?>

			<?php  $footquantity_4 = get_post_meta(get_the_ID(), 'footquantity_4', true); ?>
			<?php  $footprice_4 = get_post_meta(get_the_ID(), 'footprice_4', true); ?>

			<?php  $footquantity_5 = get_post_meta(get_the_ID(), 'footquantity_5', true); ?>
			<?php  $footprice_5 = get_post_meta(get_the_ID(), 'footprice_5', true); ?>

			<!-- mm get value form fields -->
			<?php  $mmquantity_1 = get_post_meta(get_the_ID(), 'mmquantity_1', true); ?>
			<?php  $mmprice_1 = get_post_meta(get_the_ID(), 'mmprice_1', true); ?>

			<?php  $mmquantity_2 = get_post_meta(get_the_ID(), 'mmquantity_2', true); ?>
			<?php  $mmprice_2 = get_post_meta(get_the_ID(), 'mmprice_2', true); ?>

			<?php  $mmquantity_3 = get_post_meta(get_the_ID(), 'mmquantity_3', true); ?>
			<?php  $mmprice_3 = get_post_meta(get_the_ID(), 'mmprice_3', true); ?>

			<?php  $mmquantity_4 = get_post_meta(get_the_ID(), 'mmquantity_4', true); ?>
			<?php  $mmprice_4 = get_post_meta(get_the_ID(), 'mmprice_4', true); ?>

			<?php  $mmquantity_5 = get_post_meta(get_the_ID(), 'mmquantity_5', true); ?>
			<?php  $mmprice_5 = get_post_meta(get_the_ID(), 'mmprice_5', true); ?>

			<!-- cm get value form fields -->
			<?php  $cmquantity_1 = get_post_meta(get_the_ID(), 'cmquantity_1', true); ?>
			<?php  $cmprice_1 = get_post_meta(get_the_ID(), 'cmprice_1', true); ?>

			<?php  $cmquantity_2 = get_post_meta(get_the_ID(), 'cmquantity_2', true); ?>
			<?php  $cmprice_2 = get_post_meta(get_the_ID(), 'cmprice_2', true); ?>

			<?php  $cmquantity_3 = get_post_meta(get_the_ID(), 'cmquantity_3', true); ?>
			<?php  $cmprice_3 = get_post_meta(get_the_ID(), 'cmprice_3', true); ?>

			<?php  $cmquantity_4 = get_post_meta(get_the_ID(), 'cmquantity_4', true); ?>
			<?php  $cmprice_4 = get_post_meta(get_the_ID(), 'cmprice_4', true); ?>

			<?php  $cmquantity_5 = get_post_meta(get_the_ID(), 'cmquantity_5', true); ?>
			<?php  $cmprice_5 = get_post_meta(get_the_ID(), 'cmprice_5', true); ?>

			<!-- Meter get value form fields -->
			<?php  $meterquantity_1 = get_post_meta(get_the_ID(), 'meterquantity_1', true); ?>
			<?php  $meterprice_1 = get_post_meta(get_the_ID(), 'meterprice_1', true); ?>

			<?php  $meterquantity_2 = get_post_meta(get_the_ID(), 'meterquantity_2', true); ?>
			<?php  $meterprice_2 = get_post_meta(get_the_ID(), 'meterprice_2', true); ?>

			<?php  $meterquantity_3 = get_post_meta(get_the_ID(), 'meterquantity_3', true); ?>
			<?php  $meterprice_3 = get_post_meta(get_the_ID(), 'meterprice_3', true); ?>

			<?php  $meterquantity_4 = get_post_meta(get_the_ID(), 'meterquantity_4', true); ?>
			<?php  $meterprice_4 = get_post_meta(get_the_ID(), 'meterprice_4', true); ?>

			<?php  $meterquantity_5 = get_post_meta(get_the_ID(), 'meterquantity_5', true); ?>
			<?php  $meterprice_5 = get_post_meta(get_the_ID(), 'meterprice_5', true); ?>


			<!-- Unit Prices -->
			<?php  $hemming_price = get_post_meta(get_the_ID(), 'hemming_price', true); ?>
			<?php  $accessories_price = get_post_meta(get_the_ID(), 'accessories_price', true); ?>
			<?php  $inches_price = get_post_meta(get_the_ID(), 'inches_price', true); ?>
			<?php  $foot_price = get_post_meta(get_the_ID(), 'foot_price', true); ?>
			<?php  $mm_price = get_post_meta(get_the_ID(), 'mm_price', true); ?>
			<?php  $cm_price = get_post_meta(get_the_ID(), 'cm_price', true); ?>
			<?php  $meter_price = get_post_meta(get_the_ID(), 'meter_price', true); ?>

			<!-- Inches Html Fields -->
			<input type="hidden" value="<?php echo $inchesquantity_1 ?>" id="inchesquantity_1" />
			<input type="hidden" value="<?php echo $inchesprice_1 ?>" id="inchesprice_1" />

			<input type="hidden" value="<?php echo $inchesquantity_2 ?>" id="inchesquantity_2" />
			<input type="hidden" value="<?php echo $inchesprice_2 ?>" id="inchesprice_2" />

			<input type="hidden" value="<?php echo $inchesquantity_3 ?>" id="inchesquantity_3" />
			<input type="hidden" value="<?php echo $inchesprice_3 ?>" id="inchesprice_3" />

			<input type="hidden" value="<?php echo $inchesquantity_4 ?>" id="inchesquantity_4" />
			<input type="hidden" value="<?php echo $inchesprice_4 ?>" id="inchesprice_4" />

			<input type="hidden" value="<?php echo $inchesquantity_5 ?>" id="inchesquantity_5" />
			<input type="hidden" value="<?php echo $inchesprice_5 ?>" id="inchesprice_5" />

			<!-- Foot Html Fields -->
			<input type="hidden" value="<?php echo $footquantity_1 ?>" id="footquantity_1" />
			<input type="hidden" value="<?php echo $footprice_1 ?>" id="footprice_1" />

			<input type="hidden" value="<?php echo $footquantity_2 ?>" id="footquantity_2" />
			<input type="hidden" value="<?php echo $footprice_2 ?>" id="footprice_2" />

			<input type="hidden" value="<?php echo $footquantity_3 ?>" id="footquantity_3" />
			<input type="hidden" value="<?php echo $footprice_3 ?>" id="footprice_3" />

			<input type="hidden" value="<?php echo $footquantity_4 ?>" id="footquantity_4" />
			<input type="hidden" value="<?php echo $footprice_4 ?>" id="footprice_4" />

			<input type="hidden" value="<?php echo $footquantity_5 ?>" id="footquantity_5" />
			<input type="hidden" value="<?php echo $footprice_5 ?>" id="footprice_5" />

			<!-- CM Html Fields -->
			<input type="hidden" value="<?php echo $cmquantity_1 ?>" id="cmquantity_1" />
			<input type="hidden" value="<?php echo $cmprice_1 ?>" id="cmprice_1" />

			<input type="hidden" value="<?php echo $cmquantity_2 ?>" id="cmquantity_2" />
			<input type="hidden" value="<?php echo $cmprice_2 ?>" id="cmprice_2" />

			<input type="hidden" value="<?php echo $cmquantity_3 ?>" id="cmquantity_3" />
			<input type="hidden" value="<?php echo $cmprice_3 ?>" id="cmprice_3" />

			<input type="hidden" value="<?php echo $cmquantity_4 ?>" id="cmquantity_4" />
			<input type="hidden" value="<?php echo $cmprice_4 ?>" id="cmprice_4" />

			<input type="hidden" value="<?php echo $cmquantity_5 ?>" id="cmquantity_5" />
			<input type="hidden" value="<?php echo $cmprice_5 ?>" id="cmprice_5" />
			
			<!-- MM Html Fields -->
			<input type="hidden" value="<?php echo $mmquantity_1 ?>" id="mmquantity_1" />
			<input type="hidden" value="<?php echo $mmprice_1 ?>" id="mmprice_1" />

			<input type="hidden" value="<?php echo $mmquantity_2 ?>" id="mmquantity_2" />
			<input type="hidden" value="<?php echo $mmprice_2 ?>" id="mmprice_2" />

			<input type="hidden" value="<?php echo $mmquantity_3 ?>" id="mmquantity_3" />
			<input type="hidden" value="<?php echo $mmprice_3 ?>" id="mmprice_3" />

			<input type="hidden" value="<?php echo $mmquantity_4 ?>" id="mmquantity_4" />
			<input type="hidden" value="<?php echo $mmprice_4 ?>" id="mmprice_4" />

			<input type="hidden" value="<?php echo $mmquantity_5 ?>" id="mmquantity_5" />
			<input type="hidden" value="<?php echo $mmprice_5 ?>" id="mmprice_5" />
			
			<!-- Meter Html Fields -->
			<input type="hidden" value="<?php echo $meterquantity_1 ?>" id="meterquantity_1" />
			<input type="hidden" value="<?php echo $meterprice_1 ?>" id="meterprice_1" />

			<input type="hidden" value="<?php echo $meterquantity_2 ?>" id="meterquantity_2" />
			<input type="hidden" value="<?php echo $meterprice_2 ?>" id="meterprice_2" />

			<input type="hidden" value="<?php echo $meterquantity_3 ?>" id="meterquantity_3" />
			<input type="hidden" value="<?php echo $meterprice_3 ?>" id="meterprice_3" />

			<input type="hidden" value="<?php echo $meterquantity_4 ?>" id="meterquantity_4" />
			<input type="hidden" value="<?php echo $meterprice_4 ?>" id="meterprice_4" />

			<input type="hidden" value="<?php echo $meterquantity_5 ?>" id="meterquantity_5" />
			<input type="hidden" value="<?php echo $meterprice_5 ?>" id="meterprice_5" />

			
			<!-- Hemming and Prices Fields -->
			<input type="hidden" value="<?php echo $hemming_price ?>" id="hemming_price" />
			<input type="hidden" value="<?php echo $accessories_price ?>" id="accessories_price" />
			<input type="hidden" value="<?php echo $inches_price ?>" id="inches_price" />
			<input type="hidden" value="<?php echo $foot_price ?>" id="foot_price" />
			<input type="hidden" value="<?php echo $cm_price ?>" id="cm_price" />
			<input type="hidden" value="<?php echo $mm_price ?>" id="mm_price" />
			<input type="hidden" value="<?php echo $meter_price ?>" id="meter_price" />


			<!--<input type="hidden" value="<?php //echo $packets_val ?>" id="packets_val" />
			<input type="hidden" value="<?php //echo $cases_val ?>" id="cases_val" />-->

			<div class="row widthheight_main">
				<div class="col-sm-4 ace_unit">					
					<div class="form-group" >
						<label class="control-label" for="size">Unit</label>
						<div class="">
							<select onchange="calculateQuote()" id="ace_unit" class="ace_unit form-control" name="ace_unit">					
								<option value="inches">Inches</option>
								<option value="foot" selected>Foot</option>
								<option value="mm">Milimeter</option>
								<option value="meter">Meter</option>
								<option value="cm">Centimeter</option>
							</select>	
						</div>
					</div>
				</div>
				<div class="col-sm-4 ace_width">
					<label class="control-label"  for="ace_width">Width</label>
					 <div class="cases_inner">
						<input value="2" onKeyup="calculateQuote()" placeholder="0.00" type="number" name="ace_width" class="form-control" id="ace_width" required>
					</div>		
				</div>
				<div class="col-sm-4 ace_height">
					<label class="control-label"  for="ace_height">Height</label>
					 <div class="cases_inner">
						<input value="2" onKeyup="calculateQuote()" placeholder="0.00" type="number" name="ace_height" class="form-control" id="ace_height" required>
					</div>		
				</div>
				<div class="col-sm-4 ace_hemming">
					<h3>Reinforced Hemming</h3>
					<label class="control-label" for="ace_hemming">Yes</label>
						<input value="yes" onchange="calculateQuote()" type="radio" name="ace_hemming" class="form-control">
					<label class="control-label" for="ace_hemming">No</label>
						<input value="no" onchange="calculateQuote()" type="radio" name="ace_hemming" class="form-control" checked>
				</div>
				<div class="col-sm-4 ace_accessories">
					<h3>Add Accessories</h3>
					<h3>Zip Ties Pack of 50</h3>
					<label class="control-label" for="ace_accessories">Yes</label>
						<input value="yes" onchange="calculateQuote()" type="radio" name="ace_accessories" class="form-control">
					<label class="control-label" for="ace_accessories">No</label>
						<input value="no" onchange="calculateQuote()" type="radio" name="ace_accessories" class="form-control" checked>
				</div>
				<div class="col-sm-4 ace_qty">
					<label class="control-label" for="ace_qty">Quantity</label>
					 <div class="cases_inner">
						<input value="1" onchange="calculateQuote()" onKeyup="calculateQuote()" placeholder="0.00" type="number" name="ace_qty" class="form-control" id="ace_qty" required>
					</div>		
				</div>
				<div class="ace-pricetotal">
					<label>Total Price</label>	
					<input placeholder="0.00" type="text" value="<?php echo $price_1 ?>" name="pricetotal_quote" class="form-control" id="pricetotal_quote" readonly=""> 
					<div id="total_cart_main">	
					</div>
				</div>
				<div class="col-sm-4 wh_label" >
					<label class="control-label" for="width">Sqft</label>
					 <div class="packets_inner">
						<!-- <span class="button-icon decrease packets_dec"><i class="porto-icon-minus"></i></span> -->
						<input onKeyup="calculateQuote()" placeholder="0.00" min="1" max="9999" type="number" name="ace_sqfoot" class="form-control" id="ace_sqfoot" value="1" required>
						<!-- <span class="button-icon increase packets_inc"><i class="porto-icon-plus"></i></span>  -->
					</div>	
				</div>
			</div>
			<!--<div class="row calculations_main">
				<div class="col-sm-3"><strong>Panel Cost:</strong> <span id="panel_cost_total"></span></div>
				<div class="col-sm-3"><strong>Bracket Cost:</strong> <span id="bracket_cost_total"></span></div>
				<div class="col-sm-3"><strong>Case Cost:</strong> <span id="case_cost_total"></span></div>
				<div class="col-sm-3"><strong>Processor Cost:</strong> <span id="processor_cost_total"></span></div>
			</div>-->
			

			<div style="display:none;" class="sqft_oneway">	
				<div class="form-group">
					<input placeholder="0"  type="hidden" name="sqft" class="form-control" id="sqft" readonly>									
					<input placeholder="0"  type="hidden" name="sqfttotal" class="form-control" id="sqfttotal" readonly>									
				</div>
			</div>
			<div class="row total_row_main">	
									
					<input style="display:none;" placeholder="0" type="text" name="price_per_banner" value="9.4" class="form-control" id="price_per_banner" readonly="">
					<input style="display:none;" placeholder="0" type="text" name="price" class="form-control" id="price" readonly="">
					
			</div>
			<div class="row add_cart_row_main" style="display: none;">
				<div class="col-sm-12">
					<input type="submit" name="submit_prod" value="Add to Cart" class="btn-addcart" id="btn_addcart_custom">				
				</div>		
			</div>
		
	</form>

		<!-- Table of Quantity -->
			<br>
			<div class="quantity_table_heading">Discount Table</div>
			<table class="table table-bordered table-hover table-inches">
			  <thead>
			    <tr>
			      <td scope="col">Quantity <?php echo $inchesquantity_1 ?></td>
			      <td scope="col">Quantity <?php echo $inchesquantity_2 ?></td>
			      <td scope="col">Quantity <?php echo $inchesquantity_3 ?></td>
			      <td scope="col">Quantity <?php echo $inchesquantity_4 ?></td>
			      <td scope="col">Quantity <?php echo $inchesquantity_5 ?></td>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <th colspan="1" scope="col"><?php echo $inchesprice_1 ?></th>
			      <th colspan="1" scope="col"><?php echo $inchesprice_2 ?></th>
			      <th colspan="1" scope="col"><?php echo $inchesprice_3 ?></th>
			      <th colspan="1" scope="col"><?php echo $inchesprice_4 ?></th>
			      <th colspan="1" scope="col"><?php echo $inchesprice_5 ?></th>
			    </tr>
			  </tbody>
			</table>

			<table class="table table-bordered table-hover table-foot">
			  <thead>
			    <tr>
			      <td scope="col">Quantity <?php echo $footquantity_1 ?></td>
			      <td scope="col">Quantity <?php echo $footquantity_2 ?></td>
			      <td scope="col">Quantity <?php echo $footquantity_3 ?></td>
			      <td scope="col">Quantity <?php echo $footquantity_4 ?></td>
			      <td scope="col">Quantity <?php echo $footquantity_5 ?></td>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <th colspan="1" scope="col"><?php echo $footprice_1 ?></th>
			      <th colspan="1" scope="col"><?php echo $footprice_2 ?></th>
			      <th colspan="1" scope="col"><?php echo $footprice_3 ?></th>
			      <th colspan="1" scope="col"><?php echo $footprice_4 ?></th>
			      <th colspan="1" scope="col"><?php echo $footprice_5 ?></th>
			    </tr>
			  </tbody>
			</table>

			<table class="table table-bordered table-hover table-meter">
			  <thead>
			    <tr>
			      <td scope="col">Quantity <?php echo $meterquantity_1 ?></td>
			      <td scope="col">Quantity <?php echo $meterquantity_2 ?></td>
			      <td scope="col">Quantity <?php echo $meterquantity_3 ?></td>
			      <td scope="col">Quantity <?php echo $meterquantity_4 ?></td>
			      <td scope="col">Quantity <?php echo $meterquantity_5 ?></td>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <th colspan="1" scope="col"><?php echo $meterprice_1 ?></th>
			      <th colspan="1" scope="col"><?php echo $meterprice_2 ?></th>
			      <th colspan="1" scope="col"><?php echo $meterprice_3 ?></th>
			      <th colspan="1" scope="col"><?php echo $meterprice_4 ?></th>
			      <th colspan="1" scope="col"><?php echo $meterprice_5 ?></th>
			    </tr>
			  </tbody>
			</table>
			<table class="table table-bordered table-hover table-mm">
			  <thead>
			    <tr>
			      <td scope="col">Quantity <?php echo $mmquantity_1 ?></td>
			      <td scope="col">Quantity <?php echo $mmquantity_2 ?></td>
			      <td scope="col">Quantity <?php echo $mmquantity_3 ?></td>
			      <td scope="col">Quantity <?php echo $mmquantity_4 ?></td>
			      <td scope="col">Quantity <?php echo $mmquantity_5 ?></td>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <th colspan="1" scope="col"><?php echo $mmprice_1 ?></th>
			      <th colspan="1" scope="col"><?php echo $mmprice_2 ?></th>
			      <th colspan="1" scope="col"><?php echo $mmprice_3 ?></th>
			      <th colspan="1" scope="col"><?php echo $mmprice_4 ?></th>
			      <th colspan="1" scope="col"><?php echo $mmprice_5 ?></th>
			    </tr>
			  </tbody>
			</table>
			<table class="table table-bordered table-hover table-cm">
			  <thead>
			    <tr>
			      <td scope="col">Quantity <?php echo $cmquantity_1 ?></td>
			      <td scope="col">Quantity <?php echo $cmquantity_2 ?></td>
			      <td scope="col">Quantity <?php echo $cmquantity_3 ?></td>
			      <td scope="col">Quantity <?php echo $cmquantity_4 ?></td>
			      <td scope="col">Quantity <?php echo $cmquantity_5 ?></td>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <th colspan="1" scope="col"><?php echo $cmprice_1 ?></th>
			      <th colspan="1" scope="col"><?php echo $cmprice_2 ?></th>
			      <th colspan="1" scope="col"><?php echo $cmprice_3 ?></th>
			      <th colspan="1" scope="col"><?php echo $cmprice_4 ?></th>
			      <th colspan="1" scope="col"><?php echo $cmprice_5 ?></th>
			    </tr>
			  </tbody>
			</table>
			<!-- End Table of Quantity -->

	</div>
</div>
	
<?php 
return ob_get_clean();
}

// Add custom price to cart
add_action('woocommerce_after_add_to_cart_button', 'add_check_box_to_product_page', 30 );
function add_check_box_to_product_page(){ ?>     
       <div style="margin-top:20px">           
<label for="extra_price" style="display: none;"> <?php _e( 'Extra Price', 'quadlayers' ); ?>
<input type="text" name="extra_price" id="extra_price" value=""> 
</label>
                    
</div>
     <?php
}
add_filter( 'woocommerce_add_cart_item_data', 'add_cart_item_data', 10, 3 );
 
function add_cart_item_data( $cart_item_data, $product_id, $variation_id ) {
     // get product id & price
    $product = wc_get_product( $product_id );
    $price = $product->get_price();
    // extra pack checkbox
    if( ! empty( $_POST['extra_price'] ) ) {
       
        $cart_item_data['new_price'] = $_POST['extra_price'];
    }
return $cart_item_data;
}
add_action( 'woocommerce_before_calculate_totals', 'before_calculate_totals', 10, 1 );
 
function before_calculate_totals( $cart_obj ) {
if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {
return;
}
// Iterate through each cart item
foreach( $cart_obj->get_cart() as $key=>$value ) {
if( isset( $value['new_price'] ) ) {
$price = $value['new_price'];
$value['data']->set_price( ( $price ) );
}
}
}

// add_action( 'woocommerce_before_add_to_cart_form', 'woocommerce_single_variation_add_to_cart_button', 20 );
// function woocommerce_single_variation_add_to_cart_button(){
//     echo do_shortcode('[custom-calculator]');
// }