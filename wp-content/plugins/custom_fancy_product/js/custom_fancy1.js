jQuery(document).ready(function() {


var price_1 = jQuery("#price_1").val();
jQuery("#pricetotal_quote").val(price_1);

jQuery(".table-foot").show();
jQuery(".table-inches").hide();
jQuery(".table-meter").hide();
jQuery(".table-cm").hide();
jQuery(".table-mm").hide();

calculateQuote();
function set_product_canvas(){
}

jQuery("span.button-icon.increase.packets_inc").click(function() {
	console.log("Click...");
    jQuery("#bio_packets").keydown();
    jQuery("#bio_packets").keypress();
    jQuery("#bio_packets").keyup();
    jQuery("#bio_packets").blur();
    jQuery("#bio_packets").change();
});

// Select your input element.
var negative_restrict = document.querySelector('#ace_sqfoot');

// Listen for input event on numInput.
negative_restrict.addEventListener('#ace_sqfoot', function(){
    // Let's match only digits.
    var num_digits = this.value.match(/^\d+$/);
    if (num_digits === null) {
        // If we have no match, value will be empty.
        this.value = "";
    }
}, false);


});

	function calculateQuote(){
			
		var product_price_raw = jQuery(".price span.woocommerce-Price-amount.amount bdi").html();
		
		var ace_width = jQuery("#ace_width").val();
		var ace_height = jQuery("#ace_height").val();
		var hemming_price = jQuery("#hemming_price").val();
		var hemming_selection = jQuery('input[name="ace_hemming"]:checked').val();
		var accessories_price = jQuery("#accessories_price").val();
		var accessories_selection = jQuery('input[name="ace_accessories"]:checked').val();
		//var ace_sqfoot = jQuery("#ace_sqfoot").val();

		//Inches
		var inchesquantity_1 = jQuery("#inchesquantity_1").val();
		var inchesprice_1 = jQuery("#inchesprice_1").val();
		
		var inchesquantity_2 = jQuery("#inchesquantity_2").val();
		var inchesprice_2 = jQuery("#inchesprice_2").val();

		var inchesquantity_3 = jQuery("#inchesquantity_3").val();
		var inchesprice_3 = jQuery("#inchesprice_3").val();

		var inchesquantity_4 = jQuery("#inchesquantity_4").val();
		var inchesprice_4 = jQuery("#inchesprice_4").val();

		var inchesquantity_5 = jQuery("#inchesquantity_5").val();
		var inchesprice_5 = jQuery("#inchesprice_5").val();

		//Foot
		var footquantity_1 = jQuery("#footquantity_1").val();
		var footprice_1 = jQuery("#footprice_1").val();
		
		var footquantity_2 = jQuery("#footquantity_2").val();
		var footprice_2 = jQuery("#footprice_2").val();

		var footquantity_3 = jQuery("#footquantity_3").val();
		var footprice_3 = jQuery("#footprice_3").val();

		var footquantity_4 = jQuery("#footquantity_4").val();
		var footprice_4 = jQuery("#footprice_4").val();

		var footquantity_5 = jQuery("#footquantity_5").val();
		var footprice_5 = jQuery("#footprice_5").val();
		
		//MM
		var mmquantity_1 = jQuery("#mmquantity_1").val();
		var mmprice_1 = jQuery("#mmprice_1").val();
		
		var mmquantity_2 = jQuery("#mmquantity_2").val();
		var mmprice_2 = jQuery("#mmprice_2").val();

		var mmquantity_3 = jQuery("#mmquantity_3").val();
		var mmprice_3 = jQuery("#mmprice_3").val();

		var mmquantity_4 = jQuery("#mmquantity_4").val();
		var mmprice_4 = jQuery("#mmprice_4").val();

		var mmquantity_5 = jQuery("#mmquantity_5").val();
		var mmprice_5 = jQuery("#mmprice_5").val();
		
		//CM
		var cmquantity_1 = jQuery("#cmquantity_1").val();
		var cmprice_1 = jQuery("#cmprice_1").val();
		
		var cmquantity_2 = jQuery("#cmquantity_2").val();
		var cmprice_2 = jQuery("#cmprice_2").val();

		var cmquantity_3 = jQuery("#cmquantity_3").val();
		var cmprice_3 = jQuery("#cmprice_3").val();

		var cmquantity_4 = jQuery("#cmquantity_4").val();
		var cmprice_4 = jQuery("#cmprice_4").val();

		var cmquantity_5 = jQuery("#cmquantity_5").val();
		var cmprice_5 = jQuery("#cmprice_5").val();
		
		//Meter
		var meterquantity_1 = jQuery("#meterquantity_1").val();
		var meterprice_1 = jQuery("#meterprice_1").val();
		
		var meterquantity_2 = jQuery("#meterquantity_2").val();
		var meterprice_2 = jQuery("#meterprice_2").val();

		var meterquantity_3 = jQuery("#meterquantity_3").val();
		var meterprice_3 = jQuery("#meterprice_3").val();

		var meterquantity_4 = jQuery("#meterquantity_4").val();
		var meterprice_4 = jQuery("#meterprice_4").val();

		var meterquantity_5 = jQuery("#meterquantity_5").val();
		var meterprice_5 = jQuery("#meterprice_5").val();


		var ace_qty = jQuery("#ace_qty").val();
		
		var ace_sqfoot = parseFloat(ace_width) * parseFloat(ace_height);
		jQuery("#ace_sqfoot").val(ace_sqfoot);

		//var product_price;
		//var product_price = Number(product_price_raw .replace(/[^0-9\.]+/g,""));

		//console.log("product_price.."+product_price);
		console.log("ace_sqfoot.."+ace_sqfoot);

		//var ace_sqfoot = jQuery("#ace_sqfoot").val();
		//var bio_cases =   jQuery("#bio_cases").val();
		//var bio_packets_price = parseFloat(bio_packets) * parseFloat(product_price);
		//var bio_cases_price = parseFloat(bio_cases) * parseFloat(product_price) * parseFloat(packets_per_case);
		//console.log("bio_cases_price--"+bio_cases_price);
		var ace_sel_unit = document.getElementById('ace_unit').value;
		console.log(ace_sel_unit);

		//Unit Prices
		if (ace_sel_unit == 'inches') {
			jQuery(".table-inches").show();
			jQuery(".table-foot").hide();
			jQuery(".table-meter").hide();
			jQuery(".table-cm").hide();
			jQuery(".table-mm").hide();
			var unit_price = jQuery("#inches_price").val();
			unit_price = parseFloat(unit_price) * parseFloat(ace_sqfoot);
		}
		if (ace_sel_unit == 'foot') {
			jQuery(".table-foot").show();
			jQuery(".table-inches").hide();
			jQuery(".table-meter").hide();
			jQuery(".table-cm").hide();
			jQuery(".table-mm").hide();
			var unit_price = jQuery("#foot_price").val();
			var unit_price = parseFloat(unit_price) * parseFloat(ace_sqfoot);
		}
		if (ace_sel_unit == 'meter') {
			jQuery(".table-meter").show();
			jQuery(".table-inches").hide();
			jQuery(".table-cm").hide();
			jQuery(".table-foot").hide();
			jQuery(".table-mm").hide();
			var unit_price = jQuery("#meter_price").val();
			var unit_price = parseFloat(unit_price) * parseFloat(ace_sqfoot);
		}
		if (ace_sel_unit == 'mm') {
			jQuery(".table-mm").show();
			jQuery(".table-inches").hide();
			jQuery(".table-foot").hide();
			jQuery(".table-meter").hide();
			jQuery(".table-cm").hide();
			var unit_price = jQuery("#mm_price").val();
			var unit_price = parseFloat(unit_price) * parseFloat(ace_sqfoot);
		}if (ace_sel_unit == 'cm') {
			jQuery(".table-cm").show();
			jQuery(".table-inches").hide();
			jQuery(".table-foot").hide();
			jQuery(".table-meter").hide();
			jQuery(".table-mm").hide();
			var unit_price = jQuery("#cm_price").val();
			var unit_price = parseFloat(unit_price) * parseFloat(ace_sqfoot);
		}
		console.log("unit_price.."+unit_price);

		//Unit wise Prices
		
		//Inches
		if(ace_sel_unit == 'inches'){
			//Price Qty Inches
			if (parseInt(ace_qty) == parseInt(inchesquantity_1)) {
				var qty_price = parseFloat(inchesprice_1);
			}
			if ( parseInt(ace_qty) == parseInt(inchesquantity_2)) {
				var qty_price =  parseFloat(inchesprice_2);
			}
			if ( parseInt(ace_qty) == parseInt(inchesquantity_3)) {
				var qty_price =  parseFloat(inchesprice_3);
			}
			if ( parseInt(ace_qty) == parseInt(inchesquantity_4)) {
				var qty_price =  parseFloat(inchesprice_4);
			}
			if ( parseInt(ace_qty) >= parseInt(inchesquantity_5)) {
				var qty_price =  parseFloat(inchesprice_5);
			}
		}
		//Foot
		if(ace_sel_unit == 'foot'){
			//Price Qty Inches
			if (parseInt(ace_qty) == parseInt(footquantity_1)) {
				var qty_price = parseFloat(footprice_1);
			}
			if ( parseInt(ace_qty) == parseInt(footquantity_2)) {
				var qty_price =  parseFloat(footprice_2);
			}
			if ( parseInt(ace_qty) == parseInt(footquantity_3)) {
				var qty_price =  parseFloat(footprice_3);
			}
			if ( parseInt(ace_qty) == parseInt(footquantity_4)) {
				var qty_price =  parseFloat(footprice_4);
			}
			if ( parseInt(ace_qty) >= parseInt(footquantity_5)) {
				var qty_price =  parseFloat(footprice_5);
			}
		}
		//Milimeter
		if(ace_sel_unit == 'mm'){
			//Price Qty Inches
			if (parseInt(ace_qty) == parseInt(mmquantity_1)) {
				var qty_price = parseFloat(mmprice_1);
			}
			if ( parseInt(ace_qty) == parseInt(mmquantity_2)) {
				var qty_price =  parseFloat(mmprice_2);
			}
			if ( parseInt(ace_qty) == parseInt(mmquantity_3)) {
				var qty_price =  parseFloat(mmprice_3);
			}
			if ( parseInt(ace_qty) == parseInt(mmquantity_4)) {
				var qty_price =  parseFloat(mmprice_4);
			}
			if ( parseInt(ace_qty) >= parseInt(mmquantity_5)) {
				var qty_price =  parseFloat(mmprice_5);
			}
		}
		//Centimeter
		if(ace_sel_unit == 'cm'){
			//Price Qty Inches
			if (parseInt(ace_qty) == parseInt(cmquantity_1)) {
				var qty_price = parseFloat(cmprice_1);
			}
			if ( parseInt(ace_qty) == parseInt(cmquantity_2)) {
				var qty_price =  parseFloat(cmprice_2);
			}
			if ( parseInt(ace_qty) == parseInt(cmquantity_3)) {
				var qty_price =  parseFloat(cmprice_3);
			}
			if ( parseInt(ace_qty) == parseInt(cmquantity_4)) {
				var qty_price =  parseFloat(cmprice_4);
			}
			if ( parseInt(ace_qty) >= parseInt(cmquantity_5)) {
				var qty_price =  parseFloat(cmprice_5);
			}
		}
		//Meter
		if(ace_sel_unit == 'meter'){
			//Price Qty Inches
			if (parseInt(ace_qty) == parseInt(meterquantity_1)) {
				var qty_price = parseFloat(meterprice_1);
			}
			if ( parseInt(ace_qty) == parseInt(meterquantity_2)) {
				var qty_price =  parseFloat(meterprice_2);
			}
			if ( parseInt(ace_qty) == parseInt(meterquantity_3)) {
				var qty_price =  parseFloat(meterprice_3);
			}
			if ( parseInt(ace_qty) == parseInt(meterquantity_4)) {
				var qty_price =  parseFloat(meterprice_4);
			}
			if ( parseInt(ace_qty) >= parseInt(meterquantity_5)) {
				var qty_price =  parseFloat(meterprice_5);
			}
		}

		
		//Total Price

		var dec = (qty_price/100).toFixed(2); //its convert 10 into 0.10
 		var mult = parseFloat(unit_price) * parseFloat(dec); // gives the value for subtract from main value
 		var total_price = parseFloat(unit_price) - parseFloat(mult);

 		total_price = parseFloat(total_price) * parseFloat(ace_qty);
 		
 		//Adding Hemming Price in discounted Price
 		console.log("hemming_selection..."+hemming_selection);
 		if(hemming_selection == 'yes') {
 			total_price_hem = parseFloat(ace_qty) * parseFloat(hemming_price);
 			console.log("qty_mul_hem.."+total_price_hem);
 			total_price = parseFloat(total_price_hem) + parseFloat(total_price);
 			console.log("p_with_hem...."+total_price);
 		}
 		//Adding Accessories Price in discounted Price
 		console.log("accessories_selection..."+accessories_selection);
 		if(accessories_selection == 'yes') {
 			total_price = parseFloat(total_price) + parseFloat(accessories_price);
 		}
		//var total_price = parseFloat(unit_price) - parseFloat(qty_price);
		console.log("price_range_parsed.."+qty_price);
		console.log("total_price Discounted.."+total_price);

		document.getElementById("pricetotal_quote").value = total_price;
		document.getElementById("extra_price").value = total_price.toFixed(2);
		jQuery(".price_total").html(total_price.toFixed(2));
		jQuery(".extra_price").html(total_price.toFixed(2));
		
		//if(ace_sqfoot_fixed){
			//var ace_sqfoot_fixed = ace_sqfoot;
			jQuery("#cart_ace_sqfoot").val(ace_sqfoot);
		//}
		//jQuery("#cart_cases").val(bio_cases);
		
		// jQuery.ajax({
		// 	url : ajaxurl,
		// 	type : 'POST',
		// 	data: {
  //               action: 'read_me_later',
  //               bio_packets : bio_packets,
		// 		bio_cases : bio_cases,
		// 		total_price : total_price
  //           },
		// 	success : function( response ) {
		// 		//url = response;
		// 		//jQuery("#rt_waiting").css({"display": "none"});
		// 		console.log("total_price....Ajax.."+total_price);
		// 		//window.open(url,"_self");
		// 	},
		// 	error: function(e) {
		// 	    console.log(e);
		// 	}
		// });
		


		// if (card_quantity == 100) {
		// 	card_type_rate = 8;
		// }else if (card_quantity == 200) {
		// 	card_type_rate = 16;
		// }else if(card_quantity == 500) {
		// 	card_type_rate = 36;
		// }else if(card_quantity == 1000) {
		// 	card_type_rate = 65;
		// }

		//var fpriceqtyfloat = parseFloat(card_quantity) * parseFloat(card_type_rate);
		//var fpriceqtyfloat = parseFloat(card_type_rate);
		//document.getElementById("pricetotal_oneway").value = fpriceqtyfloat;	
	}
	